package moibit

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

type NodeType int

const (
	// PrivateNode represents node on which joined
	// developer cannot create network
	PrivateNode NodeType = iota
	//PublicNode allows developers to create network on top of it
	PublicNode
	// PremiumNode are meant for premium customers
	PremiumNode
)

type RequestNodeParams struct {
	NodeName *string   `json:"nodeName"`
	NodeIP   *string   `json:"nodeIP"`
	NodeID   *string   `json:"nodeID"`
	NodeType *NodeType `json:"nodeType"`
}

type ServiceURL string

const (
	ADDNODES ServiceURL = "/nodes"
)

// SendHttpRequst sends an http request to MOIBit network.
// operation type defines the type of operation to be performed
// requestData is the data which needs to be sent
func (client *Client) SendHttpRequestNodes(serviceURL string, body []byte) ([]byte, error) {

	requestHTTP, err := http.NewRequest("POST", client.serviceURL(serviceURL), bytes.NewReader(body))
	if err != nil {
		return nil, fmt.Errorf("request generation failed: %w", err)
	}

	// Set authentication headers from the client
	client.setHeaders(requestHTTP)

	// Perform the HTTP Request
	responseHTTP, err := client.c.Do(requestHTTP)
	if err != nil {
		return nil, fmt.Errorf("request failed: %w", err)
	}

	// Check the status code of response
	if responseHTTP.StatusCode != 200 {
		return nil, fmt.Errorf("non-ok response [%v]", responseHTTP.StatusCode)
	}

	// Read all bytes from the response body
	data, err := io.ReadAll(responseHTTP.Body)
	if err != nil {
		return nil, fmt.Errorf("response data spool: %w", err)
	}

	return data, nil
}

// ReadFile reads a file from MOIBit at the given path for the given version.
// Returns the []byte data of the file and an error.
func (client *Client) AddNodes(inputNodes []RequestNodeParams) ([]byte, error) {

	nodesParamsInbytes, err := json.Marshal(&inputNodes)
	if err != nil {
		log.Fatal(err)
	}

	// Generate Request Object
	data, err := client.SendHttpRequestNodes(string(ADDNODES), nodesParamsInbytes)
	if err != nil {
		return nil, fmt.Errorf("response data spool: %w", err)
	}

	return data, nil
}

type AddNodesResponse struct {
	MetaData responseMetadata `json:"meta"`
	Data     fileMetaData     `json:"data"`
}
