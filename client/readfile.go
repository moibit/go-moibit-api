package moibit

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"
)

type ReadOperationType int

const (
	READ         ReadOperationType = 1
	VERSION      ReadOperationType = 2
	STATUS       ReadOperationType = 3
	WORLD        ReadOperationType = 4
	LIST         ReadOperationType = 5
	DeletedFiles ReadOperationType = 6
	FilesPerDir  ReadOperationType = 7
)

// SendHttpRequst sends an http request to MOIBit network.
// operation type defines the type of operation to be performed
// requestData is the data which needs to be sent
func (client *Client) SendHttpRequest(operationType ReadOperationType, fileName string, version int) ([]byte, error) {

	readOperationTypeInString := strconv.Itoa(int(operationType))

	requestHTTP, err := http.NewRequest("GET", client.serviceURL("/file"), nil)
	if err != nil {
		return nil, fmt.Errorf("request generation failed: %w", err)
	}

	log.Println("operation type : ", readOperationTypeInString)

	parm := requestHTTP.URL.Query()
	parm.Add("path", fileName)
	parm.Add("version", strconv.Itoa(version))
	parm.Add("fileOperationType", readOperationTypeInString)

	requestHTTP.URL.RawQuery = parm.Encode()
	// Set authentication headers from the client
	client.setHeaders(requestHTTP)

	// Perform the HTTP Request
	responseHTTP, err := client.c.Do(requestHTTP)
	if err != nil {
		return nil, fmt.Errorf("request failed: %w", err)
	}

	// Check the status code of response
	if responseHTTP.StatusCode != 200 {
		return nil, fmt.Errorf("non-ok response [%v]", responseHTTP.StatusCode)
	}

	// Read all bytes from the response body
	data, err := io.ReadAll(responseHTTP.Body)
	if err != nil {
		return nil, fmt.Errorf("response data spool: %w", err)
	}

	return data, nil
}

// ReadFile reads a file from MOIBit at the given path for the given version.
// Returns the []byte data of the file and an error.
func (client *Client) ReadFile(fileName string, version int) ([]byte, error) {

	// Generate Request Object
	data, err := client.SendHttpRequest(READ, fileName, version)
	if err != nil {
		return nil, fmt.Errorf("response data spool: %w", err)
	}

	return data, nil
}

type fileMetaData struct {
	Active         bool      `json:"active"`
	Enable         bool      `json:"enable"`
	Version        int       `json:"version"`
	Hash           string    `json:"hash"`
	ProvenanceHash string    `json:"provenanceHash,omitempty"`
	Filesize       int       `json:"filesize"`
	Replication    int       `json:"replication"`
	EncryptionKey  string    `json:"encryptionKey"`
	LastUpdated    time.Time `json:"lastUpdated"`
	Directory      string    `json:"directory"`
	Path           string    `json:"path"`
	NodeAddress    string    `json:"nodeAddress"`
}

type FileVersionsResponse struct {
	responseMetadata
	Data []fileMetaData `json:"data"`
}

// Version reads the given file name meta data from the MOIBit.
// Returns the []byte data of the file and an error.
func (client *Client) Version(fileName string) ([]fileMetaData, error) {
	// Generate Request Object
	data, err := client.SendHttpRequest(VERSION, fileName, 1)
	if err != nil {
		return nil, fmt.Errorf("response data spool: %w", err)
	}
	resp := new(FileVersionsResponse)
	if err := json.Unmarshal(data, resp); err != nil {
		log.Fatal(err)
	}

	return resp.Data, nil
}

type FileStatusResponse struct {
	MetaData responseMetadata `json:"meta"`
	Data     fileMetaData     `json:"data"`
}

// FileStatus returns the latest version of a file stored given a file name.
func (client *Client) Status(fileName string) (*fileMetaData, error) {
	// Generate Request Object
	data, err := client.SendHttpRequest(STATUS, fileName, 1)
	if err != nil {
		return nil, fmt.Errorf("response data spool: %w", err)
	}
	resp := new(FileStatusResponse)
	if err := json.Unmarshal(data, resp); err != nil {
		log.Fatal(err)
	}

	return &resp.Data, nil
}

type MoibitWorldResponse struct {
	MetaData responseMetadata `json:"meta"`
	Data     moibitWorld      `json:"data"`
}

type moibitWorld struct {
	Hash     string `json:"hash"`
	Privacy  int    `json:"privacy"`
	Replicas []struct {
		PeerID    string  `json:"peerID"`
		Status    string  `json:"status"`
		NodeID    string  `json:"nodeID"`
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
	} `json:"replicas"`
	Shards []struct {
		Depth int    `json:"depth"`
		Hash  string `json:"hash"`
		Size  int    `json:"size"`
	} `json:"shards"`
}

// MoiBitWorld returns the privacy , security and trust for a given filename
func (client *Client) MoiBitWorld(fileName string) (*moibitWorld, error) {
	// Generate Request Object
	data, err := client.SendHttpRequest(WORLD, fileName, 1)
	if err != nil {
		return nil, fmt.Errorf("response data spool: %w", err)
	}

	resp := new(MoibitWorldResponse)
	if err := json.Unmarshal(data, resp); err != nil {
		log.Fatal(err)
	}

	return &resp.Data, nil
}

type ListFileResponse struct {
	MetaData responseMetadata `json:"meta"`
	Data     []fileMetaData   `json:"data"`
}

// ListFiles returns the all the files active under given path
func (client *Client) List(path string) ([]fileMetaData, error) {
	// Generate Request Object
	data, err := client.SendHttpRequest(LIST, path, 1)
	if err != nil {
		return nil, fmt.Errorf("response data spool: %w", err)
	}

	resp := new(ListFileResponse)
	if err := json.Unmarshal(data, resp); err != nil {
		log.Fatal(err)
	}

	return resp.Data, nil
}

// DeletedFiles returns all the deleted files under a given path
func (client *Client) DeletedFiles(path string) ([]fileMetaData, error) {
	// Generate Request Object
	data, err := client.SendHttpRequest(DeletedFiles, path, 1)
	if err != nil {
		return nil, fmt.Errorf("response data spool: %w", err)
	}

	resp := new(ListFileResponse)
	if err := json.Unmarshal(data, resp); err != nil {
		log.Fatal(err)
	}

	return resp.Data, nil
}

// DeletedFilesPerDir returns all the files per directory or a given path
func (client *Client) DeletedFilesPerDir(path string) ([]fileMetaData, error) {
	// Generate Request Object
	data, err := client.SendHttpRequest(FilesPerDir, path, 1)
	if err != nil {
		return nil, fmt.Errorf("response data spool: %w", err)
	}

	resp := new(ListFileResponse)
	if err := json.Unmarshal(data, resp); err != nil {
		log.Fatal(err)
	}

	return resp.Data, nil
}
