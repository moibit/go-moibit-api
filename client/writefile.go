package moibit

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// requestWriteFile is the request for the WriteFile API of MOIBit
type requestWriteFile struct {
	FileText string `json:"text"`
	FileName string `json:"fileName"`

	KeepPrevious  bool `json:"keepPrevious"`
	CreateFolders bool `json:"createFolders"`
	IsProvenance  bool `json:"isProvenance"`

	Replication int            `json:"replication,omitempty"`
	Encryption  EncryptionType `json:"encryptionType,omitempty"`
}

// defaultWriteFileRequest generates a new requestWriteFile object for the given file name and data
func defaultWriteFileRequest(data []byte, name string) *requestWriteFile {
	return &requestWriteFile{
		FileName: name, FileText: string(data),
		KeepPrevious: false, CreateFolders: true, IsProvenance: false,
	}
}

// ResponseWriteFile is the response for the WriteFile API of MOIBit
type ResponseWriteFile struct {
	Metadata responseMetadata `json:"meta"`
	Data     []FileDescriptor `json:"data"`
}

// WriteOption is a request option for the WriteFile method of Client.
type WriteOption func(*requestWriteFile) error

// KeepPrevious returns a WriteOption that can be used to preserve the
// versioning of the file that is written, in case the file already exists.
func KeepPrevious() WriteOption {
	return func(request *requestWriteFile) error {
		request.KeepPrevious = true
		return nil
	}
}

// CreateFolders returns a WriteOption that can be used to specify that the file write
// should create all folders (that do not exist) specified in the path of the file name
func CreateFolders() WriteOption {
	return func(request *requestWriteFile) error {
		request.CreateFolders = true
		return nil
	}
}

// CreateOnlyFile returns a WriteOption that can be used to specify that the file write
// should fail in case folders specified in the path of the file do not exist already.
func CreateOnlyFile() WriteOption {
	return func(request *requestWriteFile) error {
		request.CreateFolders = false
		return nil
	}
}

// Provenance returns a WriteOption that can be used to specify that the
// proof of the file needs to be stored on MOI's Indus Provenance Network.
func Provenance() WriteOption {
	return func(request *requestWriteFile) error {
		request.IsProvenance = true
		return nil
	}
}

// ReplicationFactor returns a WriteOption that can be used to specify
// the number of replications for the written file on its network.
func ReplicationFactor(n int) WriteOption {
	return func(request *requestWriteFile) error {
		request.Replication = n
		return nil
	}
}

// ApplyEncryption returns a WriteOption that can specify the encryption
// scheme for the file while being written to MOIBit.
func ApplyEncryption(encryption EncryptionType) WriteOption {
	return func(request *requestWriteFile) error {
		request.Encryption = encryption
		return nil
	}
}

// WriteFile writes a given file to MOIBit. Accepts the file data as raw bytes and the file name.
// It also accepts a variadic number of WriteOption to modify the write request.
// Returns a FileDescriptor (and error) containing the status of the file after successful write.
func (client *Client) WriteFile(data []byte, name string, opts ...WriteOption) ([]fileMetaData, error) {
	// Generate Request Data
	request := defaultWriteFileRequest(data, name)
	for _, opt := range opts {
		if err := opt(request); err != nil {
			return nil, fmt.Errorf("request creation failed while applying options: %w", err)
		}
	}

	// Serialize Request Data
	requestData, err := json.Marshal(request)
	if err != nil {
		return nil, fmt.Errorf("request serialization failed: %w", err)
	}

	// Generate Request Object
	requestHTTP, err := http.NewRequest("POST", client.serviceURL("/file"), bytes.NewReader(requestData))
	if err != nil {
		return nil, fmt.Errorf("request generation failed: %w", err)
	}

	parm := requestHTTP.URL.Query()
	parm.Add("fileOperationType", "3")

	requestHTTP.URL.RawQuery = parm.Encode()
	// Set authentication headers from the client
	client.setHeaders(requestHTTP)

	// Perform the HTTP Request
	responseHTTP, err := client.c.Do(requestHTTP)
	if err != nil {
		return nil, fmt.Errorf("request failed: %w", err)
	}

	body, err := ioutil.ReadAll(responseHTTP.Body)
	if err != nil {
		log.Fatal(err)
	}

	// Decode the response into a responseWriteFiles
	response := new(FileVersionsResponse)
	if err := json.Unmarshal(body, response); err != nil {
		log.Fatal(err)
	}

	// Check the status code of response
	if response.Code != 200 {
		return nil, fmt.Errorf("non-ok response [%v]: %v", response.Code, response.Message)
	}

	// Returns the file descriptors from the response
	return response.Data, nil
}

// responseMakeDir is the response for the MakeDir API of MOIBit
type responseMakeDir struct {
	Metadata responseMetadata `json:"meta"`
	Data     string           `json:"data"`
}

// MakeDirectory creates a new directory at the given path which can than be used for storing files.
func (client *Client) MakeDirectory(path string) error {
	// Generate Request Object
	requestHTTP, err := http.NewRequest("GET", client.serviceURL("/makedir"), nil)
	if err != nil {
		return fmt.Errorf("request generation failed: %w", err)
	}

	// Set given path to query parameters
	query := requestHTTP.URL.Query()
	query.Add("path", path)
	requestHTTP.URL.RawQuery = query.Encode()

	// Set authentication headers from the client
	client.setHeaders(requestHTTP)

	// Perform the HTTP Request
	responseHTTP, err := client.c.Do(requestHTTP)
	if err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	// Decode the response into a responseMakeDir
	response := new(responseMakeDir)
	decoder := json.NewDecoder(responseHTTP.Body)
	if err := decoder.Decode(response); err != nil {
		return fmt.Errorf("response decode failed [HTTP %v]: %w", responseHTTP.StatusCode, err)
	}

	// Check the status code of response
	if response.Metadata.Code != 200 {
		return fmt.Errorf("non-ok response [%v]: %v | %v", response.Metadata.Code, response.Metadata.Message, response.Data)
	}

	return nil
}
