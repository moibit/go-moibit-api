# go-moibit-client
Golang Client Library for interacting with MOIBit's Decentralized Storage API's

The migration of enterprise data to a shared infrastructure is  exponentially increasing. At the same time the need for security, privacy, and regulatory compliance demand is going up. MoiBit is a  software protocol that bridges this gap by enabling the data privacy, security, and compliance capabilities for enterprises on a shared infrastructure such as cloud storage.

MoiBit does this by  implementing a cryptographically controlled p2p content addressed data layer  with the trust of a blockchain on top of cloud and  decentralized storage networks.

MoiBit enables personalisation at all levels : 
1. Infrastructure 
    a) Physical Nodes : Nodes could be on-premise or from across the cloud. 
    b) Network : Ability to create open and permissioned network 
    c) Applications : Ability to create application with different encryption schemes and replication factor. 
2. Privacy : 
       a) Network Encryption
       b) Developer Encryption
       c) End-User Encryption
       d) Custom Encryption
       e) End-To-End Encryption
3. Security : 
      a) Sharding 
      b) Replication Factor 
4. Cryptographic security : 
      a) Proof of storage
      b) Proof of interaction 
      c ) Proof of availability 
5. Network Decentralisation
      a) Default Replication
      b) Custom Replication
      c) Delete Protection
6. Decentralised storage
       a) Free 
       b) On demand

7. MoiBit can be used in two ways 
	   a)  Web3 Software as a service for enterprises  and businesses
       b)  Web3 decentralized storage with native support of privacy , security and trust for developers.  

