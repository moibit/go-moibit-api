package main

import (
	"encoding/json"
	"fmt"
	"log"

	moibit "gitlab.com/moibit/go-moibit-api/client"
)

func main() {

	/*** developer nonce ***/
	devNonce := "1674635891"

	/*** developer signature ***/
	devSig := "0xc28f9b849c72c6bc0776127d9637ad531c9de9996028399e9d150af5e73ae0d460b95971d2105e9beb1c86b307d2f6e52e241e59791fd2af0cb66d43425b3e4e1c"

	// default network ID from the QA environment
	networkID := "12D3KooWLdQwY4VwDD7L4oc3Wu4QA1ZmJ31x1Zcfc7TCQqHDtKUL"
	netOpts := moibit.NetworkID(networkID)
	// enter custome app ID here
	// appOpts := moibit.AppID("0x21019c5B4Bc33142909dfEf98AC4f94b2b3BA076")
	newClient, err := moibit.NewClient(devSig, devNonce, netOpts)
	if err != nil {
		log.Fatal(err)
	}

	/*** Writing an object in MoiBit with following personalizations ***
	// 1. File name [ name of the file or the key ]
	// 2. Keep previous [this means , all addition or update to a value/file/object will be recorded and maintained as a unique record (cid)]
	// 3. Replication factor [ choosing how many copies of a content need to be maintained in the network ]
	// 4. Provenance : [ability to store value/file/object in the global provenance network{MOI-Indus test} along with MoiBit ]
	// 5. Encryption : [encryption type - no encryption, network, developer, end-user, custom, end-to-end]
	***/

	object := struct {
		Data string
	}{"writing first object "}

	objectInBytes, err := json.Marshal(object)
	if err != nil {
		log.Fatal(err)
	}

	// configuring options parameters
	provenance := moibit.Provenance()
	keepPrevious := moibit.KeepPrevious()
	replication := moibit.ReplicationFactor(5)
	encryption := moibit.ApplyEncryption(3)
	// writing a file in moibit default network
	fds, err := newClient.WriteFile(objectInBytes, "/abc/my-key", provenance, keepPrevious, replication, encryption)
	if err != nil {
		log.Fatal(err)
	}

	for index, hash := range fds {
		fmt.Println("index : ", index, ", hash : ", hash.Hash, ", version : ", hash.Version)
		fmt.Println("index : ", index, ", provenance hash : ", hash.ProvenanceHash, ", version : ", hash.Version)
	}

	fmt.Println(len(fds))

	/*** fetching all versions of a file ***/
	version, err := newClient.Version("/abc/my-key")
	if err != nil {
		log.Fatal(err)
	}

	for index, val := range version {
		log.Println("index : ", index, "val", val.Hash)
	}

	/*** fetching latest version/status of a file ***/
	status, err := newClient.Status("/abc/my-key")
	if err != nil {
		log.Fatal(err)
	}

	log.Println("status : ", status.Active)
	log.Println("hash: ", status.Hash)
	log.Println("version: ", status.Version)

	/*** fetching moibit world[number of shards, replications factor] of a file ***/
	world, err := newClient.MoiBitWorld("/abc/my-key")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(" dumping moibit world : ", world)

	/*** listing all files under a directory ***/
	listfiles, err := newClient.List("/abc")
	if err != nil {
		log.Fatal(err)
	}

	for index, val := range listfiles {
		log.Println("index : ", index, "val", val.Path)
	}

	/*** listing all deleted files under a directory ***/
	deletedFiles, err := newClient.DeletedFiles("/abc")
	if err != nil {
		log.Fatal(err)
	}

	for index, val := range deletedFiles {
		log.Println("index : ", index, "val", val.Path)
	}

	/*** listing all deleted files under a directory ***/
	filesPerDir, err := newClient.DeletedFilesPerDir("/abc")
	if err != nil {
		log.Fatal(err)
	}

	for index, val := range filesPerDir {
		log.Println("index : ", index, "val", val.Path)
	}

	// example to add a node in a network
	// by default it's point to default network
	nodeParams := moibit.RequestNodeParams{}
	nodeName := "node-1"
	nodeIP := "localhost"
	nodeType := moibit.PublicNode

	nodeParams.NodeName = &nodeName
	nodeParams.NodeIP = &nodeIP
	nodeParams.NodeType = &nodeType
	nodeParams.NodeID = nil

	nodeInputParams := make([]moibit.RequestNodeParams, 0)
	nodeInputParams = append(nodeInputParams, nodeParams)

	resp, err := newClient.AddNodes(nodeInputParams)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(" dumping node response : ", string(resp))

}
